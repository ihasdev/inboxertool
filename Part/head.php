<meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>iDevs Tool</title>
  <!-- Custom fonts for this template-->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <!--<link href="https://fonts.googleapis.com/css?family=Tahoma:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">-->
  <!-- Custom styles for this template-->
  <link href="css/sb-admin-2.min.css" rel="stylesheet">

  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="Part/framework/framework.js"></script>

  <style type="text/css">

table {
  border-collapse: collapse;
  width: auto;
  overflow-x: scroll;
  display: block;
}

thead,
tbody {
  display: block;
}

tbody {
  overflow-y: scroll;
  overflow-x: hidden;
  height: 600px;
}

td,
th {
  'min-width: 150px;
  'width:auto;
  height: 30px;
  border: dashed 0px lightblue;
  overflow: hidden;
  text-overflow: ellipsis;
  max-width: 100px;
}

  .highlight {
      background: yellow;
      border: 1px solid;
      padding: 5px;
      box-shadow: 3px 3px 3px #888888;
  }

  .table td {
    padding: .75rem;
    vertical-align: middle !important;
    border-top: 1px solid #e3e6f0;
}

</style>