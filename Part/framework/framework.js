
var selectedClass;
var tableId = "";
var colorEditSign = "";

function setFieldsToTableDom(tableId,Action,DataFieldObj){

        $.ajax({
          url: 'Part/framework/ajaxframework.php',
          method: 'POST',
          dataType: 'TEXT',
          data: {
              Action: Action,
              tableId: tableId,
              DataFieldObj: DataFieldObj
          }, 
          success: function(response){

            var content = response.split(";FIELD;");
            //console.log(content[1]);
            //console.log(response);
            //console.log(content[0]);
            console.log(content[1]);
            $('#editpagevaluemodal form').html(content[0]);
            $("#"+ tableId +" thead").html(content[1]);
            $("#"+ tableId +" tbody").html(content[2]);
            addTableRowChangeColorOnClick(tableId);
            addTableAutoScrolWidth();
          },
          error: function(log) {
            //console.log(log);
        }
        });
  }

  function addTableRowChangeColorOnClick(tableId){
    $("#" + tableId + " tbody tr" ).click(function() {
      selector = "tr#" + $(this).attr('id');
      $('tbody tr').removeClass("highlight");
      $(selector).addClass("highlight");
    });
  }

  function addTableAutoScrolWidth(){
    $('table').on('scroll', function() {
      $("#" + this.id + " > *").width($(this).width() + $(this).scrollLeft());
    });
  }

  function openpagevaluemodal(clicked,field,tableId,openModalAction){ //Global Function For EventClick Openpagevaluemodal()
    oldData = field;
    tableId = tableId;

    if(openModalAction === "Create Menu"){
      $('#editpagevaluemodalLabel').html(openModalAction);
      $('#editpagevaluemodal').modal('show');
      return;
    }
    
    selectedClass = 'tr' + '#' + clicked + " "; //tr parent selector
    var i=1;
    for(var o in field){
      for(var fdrow in field[o]){
        if(fdrow === "fieldname"){
          //console.log(field[o]);
          var notiSelector = 'p#noti' + field[o][fdrow];
          var selectorData = 'td#' + field[o][fdrow];
          var modalSelector = 'input#' + field[o][fdrow];
          var modalTextareaSelector = 'textarea#' + field[o][fdrow];
          
          var selector = selectedClass + selectorData;

          //Set Modal as Page Name
          if(field[o][fdrow] === "page_tname" | field[o][fdrow] === "menu_tname" ){
            $('#editpagevaluemodalLabel').html($(selector).html());
            $('#editpagevaluemodalLabel').css({"background-color": "yellow", "padding": "5px"});
            //$('#editpagevaluemodalLabel').style.background = "yellow";
            //alert($(selector).html());
          }

          //console.log(modalSelector);#51FF0D
          $(modalSelector).val($(selector).html());
          //Alway reset back color
          $(modalSelector).css({"background-color":"white"});

          $(modalTextareaSelector).html($(selector).html());
          //console.log($(modalTextareaSelector).val());

          $(notiSelector).html("");
          $(notiSelector).removeClass("highlight");
        }
      }
    }
    
    $('#editpagevaluemodal').modal('show');
    $("tr").removeClass("highlight");
    $(selectedClass).toggleClass("highlight");
  }

  function keypressEdit(it, e, field, updateChangeFieldDom){
    var clicked = null;
    if(updateChangeFieldDom){
      //alert("In");
      it.style.background = "white";
      editpagevaluemodalSaveAndEdit(clicked, field, updateChangeFieldDom, tableId, true);
    }
    else{
      if(e.keyCode === 13){
        e.preventDefault();
        editpagevaluemodalSaveAndEdit(clicked, field, false, tableId, true);
        $('#editpagevaluemodal').modal('hide');
      }
    }
  }

  function message(){
    bootbox.alert({
        message: "This alert can be dismissed by clicking on the background!",
        backdrop: true
    });
  }

  function onFocusChangeColor(e){
    var colorUpdate = e.style.background;
    if(colorUpdate === "#FFFCBB"){
      e.style.background = "#FFFCBB";
    }else{
      e.style.background = "yellow";
    }
  }

  function resetBgColor(e){
    var colorUpdate = e.style.background;
    if(colorUpdate === "#FFFCBB"){
      e.style.background = "#FFFCBB";
    }
    else{
      e.style.background = "white";
    }
  }

  function editpagevaluemodalSaveAndEdit(clicked,field,updateChangeFieldDom,tableId, outOfFocus){
    //$('#editpagevaluemodal').modal('handleUpdate');
    if(!outOfFocus){
      $('#editpagevaluemodal').modal('hide');
    }

    var Action = "updatePage";
    var updateData = [];
    var oldData = [];
    var rowId = "";
    for(var o in field){
      //console.log(o);
      var fieldname = field[o]['fieldname'];
      var inputtype = field[o]['inputtype'];
      var status = field[o]['status'];

      var selectorData = 'td#' + field[o]['fieldname'];
      
      var modalEditedDataSelector = '';
      //alert("Here");
      if(inputtype === 'input'){
        modalEditedDataSelector = 'input#' + field[o]['fieldname'];
        //console.log(inputtype + ' if input#' + field[o]['fieldname']);
      }
      else if(inputtype === 'textarea'){
        modalEditedDataSelector = 'textarea#' + field[o]['fieldname'];
        //console.log(inputtype + ' else if textarea#' + field[o]['fieldname']);
      }
      else{
        modalEditedDataSelector = 'input#' + field[o]['fieldname'];
        //console.log('input else textarea#' + field[o]['fieldname']);
      }

      console.log(modalEditedDataSelector);

      //modalEditedDataSelector = 'input#' + field[o]['fieldname'];
      var oldDataSelector = selectedClass + selectorData;
      if(selectorData === 'td#id'){
        rowId = $(oldDataSelector).html();
      }
      var resetSelector = null;
      resetSelector = 'p#noti' + field[o]['fieldname'];
      if($(oldDataSelector).html() != $(modalEditedDataSelector).val()){
        var notiSelector = 'p#noti' + field[o]['fieldname'];
        if(inputtype === 'input') {
          $(notiSelector).html("Change from " + $(oldDataSelector).html() + " to " + $(modalEditedDataSelector).val());
          updateData.push(field[o]['fieldname']+";FIELD;"+$(modalEditedDataSelector).val());
          oldData.push($(oldDataSelector).html());
          $(modalEditedDataSelector).css('background-color','#FFFCBB');
          console.log("if");
        }
        else if(inputtype === 'textarea'){
          $(notiSelector).html("Change from " + $(oldDataSelector).html() + " to " + $(modalEditedDataSelector).val());
          updateData.push(field[o]['fieldname']+";FIELD;"+$(modalEditedDataSelector).val());
          oldData.push($(oldDataSelector).html());
          $(modalEditedDataSelector).css('background-color','#FFFCBB');
          console.log("Here");}
        else{
          $(notiSelector).html("Change from " + $(oldDataSelector).html() + " to " + $(modalEditedDataSelector).val());
          updateData.push(field[o]['fieldname']+";FIELD;"+$(modalEditedDataSelector).val());
          oldData.push($(oldDataSelector).html());
          $(modalEditedDataSelector).css('background-color','#FFFCBB');
          console.log("else");
        }
        $(notiSelector).removeClass("highlight");
        $(notiSelector).toggleClass("highlight");
      }
      else{
        $(resetSelector).html("");
        $(resetSelector).removeClass("highlight");
      }
    }

    //console.log(oldData);
    console.log('Update TABLE : ' + tableId);
    console.log('Update ID : ' + rowId);
    console.log('Old DATA : ' + oldData );
    console.log('Update DATA : ' + updateData);
    
    if(updateChangeFieldDom){
      return;
    }

    //Check if everything correct
    let updateSummaries = "";
    let i = 0;
    while(i < updateData.length){
      let dataAndField = updateData[i].split(";FIELD;");
      updateSummaries = updateSummaries + "<p>"
                      + "<mark>" + dataAndField[0] + "</mark>" + " : " 
                      + "<mark><del>" + oldData[i] + "</del></mark>" + "<strong style='style=red;'> To </strong>" + "<mark>" + dataAndField[1] + "</mark></p>" + "<br>";
      i++;
    }
    console.log(updateSummaries);
    //return;

    if(!updateSummaries){
      bootbox.alert("You do not have prepared data for updating");
    }
    else{
      bootbox.confirm( {
        message: "Summaries data prepared for updating!<br><br>"+ updateSummaries +"Do you like it? ",
        buttons: {
            confirm: {
                label: 'Yes',
                className: 'btn-success'
            },
            cancel: {
                label: 'No',
                className: 'btn-danger'
            }
        },
        callback: function (result) {
          
          if(result){
            
            //console.log('This was logged in the callback: ' + result);
            $.ajax({
              url: 'Part/framework/ajaxframework.php',
              method: 'POST',
              dataType: 'TEXT',
              data: {
                  Action: Action,
                  tableId: tableId,
                  rowId: rowId,
                  updateData: updateData
              }, 
              success: function(response){
                console.log(updateData);
                var row = 0;
                //console.log(updateData.length);
                let updateDomField;
                let updateSelectorDom;
                let updateModalDomOnUpdate;
                while(row < updateData.length){
                  updateDomField =  updateData[row].split(";FIELD;");
                  //tr#field-id-9
                  updateSelectorDom = 'tr#field-id-'+ rowId + ' td#' + updateDomField[0];
                  updateModalDomOnUpdate = ".modal-body #" + updateDomField[0];
                  $(updateSelectorDom).html(updateDomField[1]);
                  $(updateModalDomOnUpdate).css({"background-color": "#51FF0D"});
                  colorEditSign = "#51FF0D";
                  console.log(updateSelectorDom);
                  row++;
                }
              },
              error: function(log) {
                console.log(log);
            }
            });
            //End Ajax
          }
        }
  
      });

    }
    

    


  }
  
