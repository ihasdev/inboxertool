<div class="container-fluid">
          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Inboxer - Autopost Generator</h1>
          </div>

          <div class="container">
          <form>
            <div class="form-group row">
              <label for="campaignname" class="col-4 col-form-label">Campaign's Name</label> 
              <div class="col-8">
                <input id="campaignname" name="campaignname" type="text" class="form-control" required="required">
              </div>
            </div>

            <div class="form-group row">
              <label for="pageselection" class="col-4 col-form-label">Company Page</label> 
              <div class="col-8">
                <select id="pageselection" name="pageselection" class="custom-select" data-live-search="true">
                </select>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-4">Membership Type</label> 
              <div class="col-8">
                <div class="custom-control custom-radio custom-control-inline">
                  <input name="radio" id="radio_0" type="radio" class="custom-control-input" value="1"> 
                  <label for="radio_0" class="custom-control-label">Free</label>
                </div>
                <div class="custom-control custom-radio custom-control-inline">
                  <input name="radio" id="radio_1" type="radio" class="custom-control-input" value="2"> 
                  <label for="radio_1" class="custom-control-label">Gold</label>
                </div>
                <div class="custom-control custom-radio custom-control-inline">
                  <input name="radio" id="radio_2" type="radio" class="custom-control-input" value="3"> 
                  <label for="radio_2" class="custom-control-label">Diamond</label>
                </div>
              </div>
            </div> 

            <div class="form-group row">
              <label class="col-4">Add In Content</label> 
              <div class="col-8">
                <div class="custom-control custom-checkbox custom-control-inline">
                  <input name="addincontent" id="addincontent_0" type="checkbox" class="custom-control-input" value="all"> 
                  <label for="addincontent_0" class="custom-control-label">All</label>
                </div>
                <div class="custom-control custom-checkbox custom-control-inline">
                  <input name="addincontent" id="addincontent_1" type="checkbox" class="custom-control-input" value="unselect"> 
                  <label for="addincontent_1" class="custom-control-label">Unselect</label>
                </div>
                <div class="custom-control custom-checkbox custom-control-inline">
                  <input name="addincontent" id="addincontent_2" type="checkbox" class="custom-control-input" value="quote"> 
                  <label for="addincontent_2" class="custom-control-label">Quote</label>
                </div>
                <div class="custom-control custom-checkbox custom-control-inline">
                  <input name="addincontent" id="addincontent_3" type="checkbox" class="custom-control-input" value="videolink"> 
                  <label for="addincontent_3" class="custom-control-label">Youtube Video Link</label>
                </div>
                <div class="custom-control custom-checkbox custom-control-inline">
                  <input name="addincontent" id="addincontent_4" type="checkbox" class="custom-control-input" value="newslink"> 
                  <label for="addincontent_4" class="custom-control-label">News Link</label>
                </div>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-4">Brands/Categories</label> 
              <div class="col-8">
                <div class="custom-control custom-checkbox custom-control-inline">
                  <input name="brand_id" id="brand_id_0" type="checkbox" class="custom-control-input" value="all" required="required"> 
                  <label for="brand_id_0" class="custom-control-label">All</label>
                </div>
                <div class="custom-control custom-checkbox custom-control-inline">
                  <input name="brand_id" id="brand_id_1" type="checkbox" class="custom-control-input" value="unselect" required="required"> 
                  <label for="brand_id_1" class="custom-control-label">Unselect</label>
                </div>
                <div class="custom-control custom-checkbox custom-control-inline">
                  <input name="brand_id" id="brand_id_2" type="checkbox" class="custom-control-input" value="apple" checked="checked" required="required"> 
                  <label for="brand_id_2" class="custom-control-label">Apple</label>
                </div>
                <div class="custom-control custom-checkbox custom-control-inline">
                  <input name="brand_id" id="brand_id_3" type="checkbox" class="custom-control-input" value="samsung" checked="checked" required="required"> 
                  <label for="brand_id_3" class="custom-control-label">Samsung</label>
                </div>
                <div class="custom-control custom-checkbox custom-control-inline">
                  <input name="brand_id" id="brand_id_4" type="checkbox" class="custom-control-input" value="oppo" checked="checked" required="required"> 
                  <label for="brand_id_4" class="custom-control-label">Oppo</label>
                </div>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-4">Products/Year</label> 
              <div class="col-8">
                <div class="custom-control custom-checkbox custom-control-inline">
                  <input name="product_by_year" id="product_by_year_0" type="checkbox" class="custom-control-input" value="all"> 
                  <label for="product_by_year_0" class="custom-control-label">All</label>
                </div>
                <div class="custom-control custom-checkbox custom-control-inline">
                  <input name="product_by_year" id="product_by_year_1" type="checkbox" class="custom-control-input" value="unselect"> 
                  <label for="product_by_year_1" class="custom-control-label">Unselect</label>
                </div>
                <div class="custom-control custom-checkbox custom-control-inline">
                  <input name="product_by_year" id="product_by_year_2" type="checkbox" checked="checked" class="custom-control-input" value="iphone2019"> 
                  <label for="product_by_year_2" class="custom-control-label">IPHONE 2019</label>
                </div>
                <div class="custom-control custom-checkbox custom-control-inline">
                  <input name="product_by_year" id="product_by_year_3" type="checkbox" checked="checked" class="custom-control-input" value="iphone2018"> 
                  <label for="product_by_year_3" class="custom-control-label">IPHONE 2018</label>
                </div>
                <div class="custom-control custom-checkbox custom-control-inline">
                  <input name="product_by_year" id="product_by_year_4" type="checkbox" checked="checked" class="custom-control-input" value="iphone2017"> 
                  <label for="product_by_year_4" class="custom-control-label">IPHONE 2017</label>
                </div>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-4">Products/Brand&Year</label> 
              <div class="col-8">
                <div class="custom-control custom-checkbox custom-control-inline">
                  <input name="product_by_brand_year" id="product_by_brand_year_0" type="checkbox" required="required" class="custom-control-input" value="all"> 
                  <label for="product_by_brand_year_0" class="custom-control-label">All</label>
                </div>
                <div class="custom-control custom-checkbox custom-control-inline">
                  <input name="product_by_brand_year" id="product_by_brand_year_1" type="checkbox" required="required" class="custom-control-input" value="unselect"> 
                  <label for="product_by_brand_year_1" class="custom-control-label">Unselect</label>
                </div>
                <div class="custom-control custom-checkbox custom-control-inline">
                  <input name="product_by_brand_year" id="product_by_brand_year_2" type="checkbox" checked="checked" required="required" class="custom-control-input" value="iphonexs"> 
                  <label for="product_by_brand_year_2" class="custom-control-label">iPhone Xs</label>
                </div>
                <div class="custom-control custom-checkbox custom-control-inline">
                  <input name="product_by_brand_year" id="product_by_brand_year_3" type="checkbox" checked="checked" required="required" class="custom-control-input" value="iphonex"> 
                  <label for="product_by_brand_year_3" class="custom-control-label">iPhone X</label>
                </div>
                <div class="custom-control custom-checkbox custom-control-inline">
                  <input name="product_by_brand_year" id="product_by_brand_year_4" type="checkbox" checked="checked" required="required" class="custom-control-input" value="iphonexr"> 
                  <label for="product_by_brand_year_4" class="custom-control-label">iPhone Xr</label>
                </div>
              </div>
            </div> 
            <div class="form-group row">
              <div class="offset-4 col-8">
                <button name="submit" type="submit" class="btn btn-primary">Generate Post</button>
              </div>
            </div>

          </form>
        </div>
          

</div>