<!DOCTYPE html>
<html lang="en">
<head>
<?php 
    include("Part/head.php"); 
?>
</head>
<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">
    <!-- Sidebar --> <?php include("Part/sidebar.php"); ?> <!-- End of Sidebar -->
    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">
      <!-- Main Content -->
      <div id="content">
        <!-- Topbar --><?php include("Part/topbar.php"); ?><!-- End of Topbar -->   
        
<!-- -------------------------------Begin Page Content------------------------------- --> 
        <?php //include("Part/inboxertool/addcompanydetailcontent.php"); ?> 
        <div class="container-fluid">
          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Edit Customer's Page Details</h1>
          </div>

          <div class="form-group row">
              <div class="col-3">
                <button id="updatelatestcustomer" name="submit" type="button" value="selectall" class="btn btn-primary" data-toggle="modal" data-target="#myModal">Get Latest Pages</button>
              </div>
          </div>

          <script type="text/javascript"> 
          // o = {
          //   fieldone:{'firstname': "Firstname 1", 'lastname': "Lastname 1"},
          //   fieldtwo:{'firstname': "Firstname 2", 'lastname': "Lastname 2", 'age': 18},
          //   fieldthree:{'firstname': "Firstname 3", 'lastname': "Lastname 3", 'age': 10}
          // };
          // for(var key in o){
          //   //console.log(o[key]);
          //   for(var k in o[key]){
          //     //console.log(k);
          //     //console.log(k + ' = ' + o[key][k]);
          //   }
          // }
              var tableId = "extend_facebook_page_detail";
              fields = {  
                      f1:{'fieldname':"id", 'inputtype': "input", 'status': "disabled"}, 
                      f2:{'fieldname': "page_tid", 'inputtype': "input", 'status': "disabled"},
                      f3:{'fieldname': "cu_tname", 'inputtype': "input", 'status': "enable"},
                      f4:{'fieldname': "page_tname", 'inputtype': "textarea", 'status': "disabled"},
                      f5:{'fieldname': "page_tnumber", 'inputtype': "input", 'status': "enable"},
                      f6:{'fieldname': "page_taddress", 'inputtype': "textarea", 'status': "enable"},
                      f7:{'fieldname': "fb_tid", 'inputtype': "input", 'status': "disabled"},
                      f8:{'fieldname': "page_talias", 'inputtype': "input", 'status': "enable"},
                      f9:{'fieldname': "content", 'inputtype': "input", 'status': "enable"},
                      f10:{'fieldname': "brand_cat", 'inputtype': "input", 'status': "enable"},
                      f11:{'fieldname': "pro_by_year", 'inputtype': "input", 'status': "enable"},
                      f12:{'fieldname': "pro_by_brand_year", 'inputtype': "input", 'status': "enable"},
                      f13:{'fieldname': "own_by", 'inputtype': "input", 'status': "enable"},
                      f14:{'fieldname': "member_ttype", 'inputtype': "input", 'status': "enable"}
                     };
            setFieldsToTableDom(tableId,"getFields",fields); 
          </script>

          <div class="row">
          <div class="col-md-12">
            <div class="table-responsive">
            <table class="table table-hover" id="extend_facebook_page_detail">
              <thead class="thead-dark">
              </thead>
              <tbody>
              </tbody>
            </table>
            </div>
          </div>
          </div>

          <!-- Modal for messaging template-->
          <div class="modal fade" id="myModal">
            <div class="modal-dialog">
              <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                  <h4 class="modal-title" id="modal_header_tittle"></h4>
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <!-- Modal body -->
                <div id="modal_message" class="modal-body">
                </div>
                <!-- Modal footer -->
                <div class="modal-footer">
                  <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
              </div>
            </div>
          </div>
          <!-- The Modal -->

          <!-- Modal for Edit Page Value -->
          <div class="modal fade" id="editpagevaluemodal" data-keyboard="false" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="editpagevaluemodalLabel"></h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <form></form>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  <button type="button" class="btn btn-primary" id="editpagevaluemodalSaveAndEdit" onClick="editpagevaluemodalSaveAndEdit(this.id,fields,null,tableId)" >Update</button>
                </div> 

              </div>
            </div>
          </div>

</div>
        
<!-- -------------------------------/.container-fluid------------------------------- -->

      </div>
      <!-- End of Main Content -->
      <!-- Footer --> <?php include("Part/foot.php"); ?> <!-- End of Footer -->
    </div><!-- End of Content Wrapper -->
  </div><!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

<?php include("Part/logoutmodal.php"); ?>
<?php include("Part/jsfoot.php"); ?>

</body>

</html>
