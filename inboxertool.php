<?php 
    require 'connection.php';
    $sql = $cn->query("Select id, page_id, page_name FROM facebook_rx_fb_page_info");

    //$data = $sql->fetch_array();
    // if($sql->num_rows > 0){
    //     $data = $sql->fetch_array();
    //     var_dump($data);
    // }
    //var_dump($data);

?>

<!DOCTYPE html>
<html lang="en">
<head>
<?php 
    include("Part/head.php"); 
?>

</head>
<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">
    <!-- Sidebar --> <?php include("Part/sidebar.php"); ?> <!-- End of Sidebar -->
    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">
      <!-- Main Content -->
      <div id="content">
        <!-- Topbar --><?php include("Part/topbar.php"); ?><!-- End of Topbar -->
        <!-- Begin Page Content -->  <?php include("Part/inboxertool/inboxertoolcontent.php"); ?> <!-- /.container-fluid -->
      </div>
      <!-- End of Main Content -->

      <!-- Footer --> <?php include("Part/foot.php"); ?> <!-- End of Footer -->
    </div><!-- End of Content Wrapper -->

  </div><!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

<?php include("Part/logoutmodal.php"); ?>
<?php include("Part/jsfoot.php"); ?>

<script type="text/javascript">
    $(document).ready(function(){
        getAllFbPage('getAllFbPage');
    });

    function getAllFbPage(key){
        $.ajax({
            url: 'ajax.php',
            method: 'POST',
            dataType: 'text',
            data: {
                key: key
            }, success: function(response){
                if (key == "getAllFbPage"){
                    //console.log(response);
                    $('#pageselection').append(response);
                }
            }
        });
    }
</script>

</body>

</html>
