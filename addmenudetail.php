<!DOCTYPE html>
<html lang="en">
<head>
<?php 
    include("Part/head.php"); 
?>
</head>
<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">
    <!-- Sidebar --> <?php include("Part/sidebar.php"); ?> <!-- End of Sidebar -->
    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">
      <!-- Main Content -->
      <div id="content">
        <!-- Topbar --><?php include("Part/topbar.php"); ?><!-- End of Topbar -->   
        
<!-- -------------------------------Begin Page Content------------------------------- --> 
        <?php //include("Part/inboxertool/addcompanydetailcontent.php"); ?> 
        <div class="container-fluid">
          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Edit Customer's Page Details</h1>
          </div>

          <div class="form-group row">
              <div class="col-3">
                <button id="updatelatestcustomer" name="submit" type="button" value="selectall" class="btn btn-primary" data-toggle="modal" data-target="#myModal" disabled>Get Latest Pages</button>
              </div>

              <div class="col-3">
                <button onclick="openpagevaluemodal(this.id,fields,tableId,'Create Menu')" id="addnewmenu" name="submit" type="button" class="btn btn-primary" >New Menu</button>
              </div>

          </div>

          <div class="row" id="table">
          <div class="col-md-12">
            <div class="table-responsive">
            <table class="table table-hover">
              <thead class="thead-dark">
              </thead>
              <tbody>
              </tbody>
            </table>
            </div>
          </div>
          </div>

          <script type="text/javascript">
              var tableId = "extend_fb_menu";
              fields = {  
                      f1:{'fieldname': "id", 'inputtype': "input", 'status': "disabled"}, 
                      f2:{'fieldname': "menu_tname", 'inputtype': "input", 'status': "enable"},
                      f3:{'fieldname': "parent", 'inputtype': "input", 'status': "enable"},
                      f4:{'fieldname': "menu_turl", 'inputtype': "textarea", 'status': "enable"},
                      f5:{'fieldname': "menu_tfilename", 'inputtype': "input", 'status': "enable"}
                     };
            setFieldsToTableDom(tableId,"getFields",fields); 
              $('table').attr('id',tableId);
          </script>

          <!-- Modal for messaging template-->
          <div class="modal fade" id="myModal">
            <div class="modal-dialog">
              <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                  <h4 class="modal-title" id="modal_header_tittle"></h4>
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <!-- Modal body -->
                <div id="modal_message" class="modal-body">
                </div>
                <!-- Modal footer -->
                <div class="modal-footer">
                  <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
              </div>
            </div>
          </div>
          <!-- The Modal -->

          <!-- Modal for Edit Page Value -->
          <div class="modal fade" id="editpagevaluemodal" data-keyboard="false" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="editpagevaluemodalLabel"></h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <form></form>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  <button type="button" class="btn btn-primary" id="editpagevaluemodalSaveAndEdit" onClick="editpagevaluemodalSaveAndEdit(this.id,fields,null,tableId)" >Add/Update</button>
                </div> 

              </div>
            </div>
          </div>

</div>
        
<!-- -------------------------------/.container-fluid------------------------------- -->

      </div>
      <!-- End of Main Content -->
      <!-- Footer --> <?php include("Part/foot.php"); ?> <!-- End of Footer -->
    </div><!-- End of Content Wrapper -->
  </div><!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

<?php include("Part/logoutmodal.php"); ?>
<?php include("Part/jsfoot.php"); ?>

</body>

</html>
