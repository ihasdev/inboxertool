<!DOCTYPE html>
<html lang="en">
<head>
<?php include("Part/head.php"); ?>
</head>
<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">
    <!-- Sidebar --> <?php include("Part/sidebar.php"); ?> <!-- End of Sidebar -->
    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">
      <!-- Main Content -->
      <div id="content">
        <!-- Topbar --><?php include("Part/topbar.php"); ?><!-- End of Topbar -->
        <!-- Begin Page Content -->  <?php include("Part/pagecontent.php"); ?> <!-- /.container-fluid -->
      </div>
      <!-- End of Main Content -->

      <!-- Footer --> <?php include("Part/foot.php"); ?> <!-- End of Footer -->
    </div><!-- End of Content Wrapper -->

  </div><!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

<?php include("Part/logoutmodal.php"); ?>
<?php include("Part/jsfoot.php"); ?>

</body>

</html>
